from Vector import Vector2D
from Colour import Colour, colors, colors_index
import random

class Particle:
	def __init__(self, radius, pos, speed, colour):
		x, y = pos
		vx, vy = speed
		self.r = radius		
		self.p = Vector2D(x,y)
		self.v = Vector2D(vx,vy)
		self.col = colour
		self.counter = random.randint(0,600)
	
	def cycle_color(self):
		self.col = colors[(colors_index[self.col]+1) % len(colors)]
		
	def move(self, delta):
		self.p += self.v*delta
		if self.col == 'CYAN' and abs(self.v.x) < 500:
			self.v.x *= 1.02
		elif self.col == 'GREEN' and abs(self.v.y) < 500:
			self.v.y *= 1.02
		elif self.col == 'RED' and abs(self.v.x) > 50:
			self.v.x /= 1.02
		elif self.col == 'WHITE' and abs(self.v.y) > 50:
			self.v.y /= 1.02
		elif self.col == 'YELLOW' and abs(self.r) < 100:
			self.r *= 1.005
		elif self.col == 'MAGENTA' and abs(self.r) > 5:
			self.r /= 1.005
		elif self.col == 'BLUE' and abs(self.v.x) < 500 and abs(self.v.y) < 500:
			self.v.x *= 1.02
			self.v.y *= 1.02
		elif self.col == 'BLACK' and abs(self.v.x) > 50 and abs(self.v.y) > 50:
			self.v.x /= 1.02
			self.v.y /= 1.02
		if self.counter % 600 == 0:
			self.cycle_color()
		self.counter += 1
		
		
	def bounce(self, box):
		xmin, xmax, ymin, ymax = box

		if self.p.x - self.r < xmin:
			self.p.x = 2*xmin - self.p.x + 2*self.r
			self.v.x *= -1
			self.cycle_color()
			
		if self.p.x + self.r > xmax:
			self.p.x = 2*xmax - self.p.x - 2*self.r
			self.v.x *= -1
			self.cycle_color()
			
		if self.p.y - self.r < ymin:
			self.p.y = 2*ymin - self.p.y + 2*self.r
			self.v.y *= -1
			self.cycle_color()
			
		if self.p.y + self.r > ymax:
			self.p.y = 2*ymax - self.p.y - 2*self.r
			self.v.y *= -1
			self.cycle_color()
