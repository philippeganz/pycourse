class Window:
	def __init__(self, x, y, width, height):
		self._x = x + width/2
		self._y = y + height/2
		self._width = width
		self._height = height
		
	@property
	def x(self):
		return self._x
	
	@property
	def y(self):
		return self._y
		
	@property
	def width(self):
		return self._width
		
	@property
	def height(self):
		return self._height
		
	def area(self):
		return self._width * self._height
		
	def perimeter(self):
		return 2*self._width + 2*self._height
