from Window import Window
from pytest import raises, mark

def test_construct():
	x, y, width, height = 1,2,3,4
	Window(x, y, width, height)
	
def test_x():
	x, y, width, height = 1,2,3,4
	w = Window(x, y, width, height)
	assert w.x == x + width/2
	
def test_y():
	x, y, width, height = 1,2,3,4
	w = Window(x, y, width, height)
	assert w.y == y + height/2
	
def test_width():
	x, y, width, height = 1,2,3,4
	w = Window(x, y, width, height)
	assert w.width == width

def test_height():
	x, y, width, height = 1,2,3,4
	w = Window(x, y, width, height)
	assert w.height == height
	
def test_x_read_only():
	x, y, width, height = 1,2,3,4
	w = Window(x, y, width, height)
	with raises(AttributeError):
		w.x = 3

def test_y_read_only():
	x, y, width, height = 1,2,3,4
	w = Window(x, y, width, height)
	with raises(AttributeError):
		w.y = 3
		
@mark.parametrize(' x,y,width,height,area',
				  ((2,3,4,    5,     20),
				   (1,2,3,    4,     12)))
def test_area_returns_area(x,y,width,height,area):
	w = Window(x, y, width, height)
	assert w.area() == area
	
@mark.parametrize(' x,y,width,height,perimeter',
				  ((2,3,4,    5,     18),
				   (1,2,3,    4,     14)))
def test_area_returns_perimeter(x,y,width,height,perimeter):
	w = Window(x, y, width, height)
	assert w.perimeter() == perimeter
		
