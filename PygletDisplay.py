import pyglet
from math import sin, cos, pi, sqrt
from Colour import Colour

class PygletDisplay:
	def __init__(self, width=800, height=600):
		self.width = width
		self.height = height
		self.window = pyglet.window.Window(width,height)
		self.window.event(self.on_draw)
		self.objects = None
		
	def draw_circle(self, center, radius):
		delta_angle = pi / 10
		angle = 0
		while angle < 2*pi:
			yield center.x + radius * cos(angle)
			yield center.y + radius * sin(angle)
			angle += delta_angle
	
	def on_draw(self):
		self.window.clear()
		
		for obj in self.objects:
			pyglet.gl.glColor3f(* getattr(Colour,obj.col.upper()).as_rgb_01())
			pyglet.graphics.draw(20, pyglet.gl.GL_LINE_LOOP,('v2f', tuple(self.draw_circle(obj.p,obj.r))))
	
	def update(self, dt):
		for obj in self.objects:
			obj.move(dt)
			obj.bounce((0, self.width, 0, self.height))
	
	def start(self, objects):
		self.objects = objects
		pyglet.clock.schedule_interval(self.update, 1/60.0)
		pyglet.app.run()
