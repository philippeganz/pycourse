import tkinter

class TkinterDisplay:
	def __init__(self, width=800, height=600):
		self.width = width
		self.height = height
		master = tkinter.Tk()
		self.window = tkinter.Canvas(master, width=width, height=height, bg='black')
		self.window.pack()
		self.objects = None
		
	def draw_circle(self, center, radius, color):
		self.window.create_oval(center.x, center.y, center.x+2*radius, center.y+2*radius, outline=color)
		
	def update(self, dt):
		self.window.delete(tkinter.ALL)
		for obj in self.objects:
			obj.move(dt)
			obj.bounce((0-obj.r, self.width-obj.r, 0-obj.r, self.height-obj.r))
			self.draw_circle(obj.p, obj.r, obj.col.lower())
		self.window.after(17, self.update, 1/60.0)
	
	def start(self, objects):
		self.objects = objects
		self.update(0)
		tkinter.mainloop()
