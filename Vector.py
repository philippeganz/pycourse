from math import sqrt
from operator import eq

def Vector(dims):

	class VectorGen:
	
		def __init__(self, *values):
			if len(values) != len(dims):
				raise TypeError
			self.coord = list(values)

		def __add__(self, other):
			return VectorGen(* (x + y for x, y in zip(self.coord, other.coord)))
		
		def __sub__(self, other):
			return VectorGen(* (x - y for x, y in zip(self.coord, other.coord)))
		
		def __neg__(self):
			return VectorGen(* (x * (-1) for x in self.coord))
		
		def __mul__(self, num):
			return VectorGen(* (x * num for x in self.coord))
		
		__rmul__ = __mul__
		
		def __div__(self, num):
			return VectorGen(* (x / num for x in self.coord))
		
		__truediv__ = __div__
		
		def __abs__(self):
			return sqrt(sum([x*x for x in self.coord]))
			
		def __getitem__(self, index):
			return self.coord[index]
			
		def __setitem__(self, index, value):
			self.coord[index] = value
			
		def __eq__(self, other):
			return eq(self.coord, other.coord)
			
		def __str__(self):
			return f'{VectorGen.__name__}{tuple(self.coord)}'
			
		__repr__ = __str__
			
	def get_and_set(index):
		def getter(self):
			return self.coord[index]
		def setter(self, value):
			self.coord[index] = value
		return getter,setter
		
	for i in range(0,len(dims)):
		setattr(VectorGen, dims[i], property(*get_and_set(i)))

	VectorGen.__name__ = f"Vector('{dims}')"
	
	return VectorGen
		
Vector2D = Vector('xy')
Vector2D.__name__ = 'Vector2D'
Vector3D = Vector('xyz')
Vector3D.__name__ = 'Vector3D'

