from sys import argv
if 'pyg' in argv:
	from PygletDisplay import PygletDisplay as Display
elif 'tk' in argv:
	from TkinterDisplay import TkinterDisplay as Display
from Particle import Particle
from Colour import colors
import random
from enum import Enum

width = 960
height = 1080
display = Display(width, height)
particle_amount = 50
max_speed = 100.0
obj_to_draw = []
while particle_amount > 0:
	obj_to_draw.append(Particle(random.randint(5,50), (width, height), (random.uniform(-max_speed,max_speed), random.uniform(-max_speed,max_speed)), colors[random.randint(0,len(colors)-1)]))
	particle_amount -= 1

display.start(obj_to_draw)
